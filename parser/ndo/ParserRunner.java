package ndo;

import static java.lang.System.out;

public class ParserRunner {

	static void parseFlagsAndDoSomethingThatIsHopefullyTheCorrectThingToDo(String[] args) {
		boolean printHelp = false;
		boolean printTables = false;
		boolean printHumans = false;
		String filename = "";

		for (String s : args) {
			if (s.equals("-h")) {
				printHelp = true;
			} else if (s.equals("-t")) {
				printTables = true;
			} else if (s.equals("-s")) {
				printHumans = true;
			} else {
				if (filename.length() == 0) {
					filename = s;
				}
			}
		}

		if (printHelp) {
			out.println("  Usage:\n    ./llgen <flags*> <filename> <flags*>\n");
			out.println("  Flags:");
			out.println("    -h\tprint help");
			out.println("    -t\tprint YAML tables");
			out.println("    -s\tprint in human-readable format");
		}

		if (filename.length() > 0) {
			callParser(filename, printTables, printHumans);
		}
	}

	static void callParser(String f, boolean printTables, boolean printHumans) {
		Parser parser = new Parser(f);
		if (printTables) {
			parser.printTables();
		}
		if (printHumans) {
			parser.printHumans();
		}
	}

	public static void main(String[] args) {
		parseFlagsAndDoSomethingThatIsHopefullyTheCorrectThingToDo(args);
	}

}
