package ndo;

public class SchedulerRunner {

	static void printUsage() {
		String space = " ";
		System.err.println("Usage:");
		System.err.printf("%2s%s\n", space, "./sched <flags>* <filename> <flags>*");
		System.err.println("Flags:");
		System.err.printf("%2s%-10s%s\n", space, "-h", "print valid commands");
		System.err.printf("%2s%-10s%s\n", space, "nodes", "print [label: instruction] (using vrs instead of srs)");
		System.err.printf("%2s%-10s%s\n", space, "edges", "print [label: { labels }]");
		System.err.printf("%2s%-10s%s\n", space, "weights", "print [label: latency weight]");
	}

	static void checkFlags(String[] flags) {
		if (flags.length < 1) {
			printUsage();
		}

		String filename = null;
		// boolean printEdges = false;
		// boolean printNodes = false;
		// boolean printWeights = false;
		for (String f : flags) {
			switch (f.toLowerCase()) {
			case "-h":
				printUsage();
				return;
				// case "nodes":
				// printNodes = true;
				// break;
				// case "edges":
				// printEdges = true;
				// break;
				// case "weights":
				// printWeights = true;
				// break;
			default:
				filename = (filename == null) ? f : filename;
			}
		}
		schedule(filename);
	}

	static void schedule(String filename) {
		Scheduler s = new Scheduler(filename);
		s.printNodes();
		System.out.println();
		s.printEdges();
		System.out.println();
		s.printWeights();
	}

	public static void main(String[] args) {
		checkFlags(args);
	}

}
