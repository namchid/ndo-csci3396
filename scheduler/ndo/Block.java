package ndo;

public class Block {

	OPCODE opcode;
	Op op1;
	Op op2;
	Op op3;
	final int INFINITY = Integer.MAX_VALUE;
	String label;

	public Block(OPCODE opcode, Op op1, Op op2, Op op3, String label) {
		this.opcode = opcode;
		this.op1 = op1;
		this.op2 = op2;
		this.op3 = op3;
		this.label = label;
	}

	public Block() {
		opcode = null;
		op1 = null;
		op2 = null;
		op3 = null;
		label = "";
	}

	public String headerString() {
		String oc = String.format("%-10s", "OPCODE");
		String div = " | ";
		String sr = String.format("%-10s", "SR");
		String vr = String.format("%-10s", "VR");
		String pr = String.format("%-10s", "PR");
		String nu = String.format("%-10s", "SR");

		return oc + div + sr + vr + pr + nu + div + sr + vr + pr + nu + div + sr + vr + pr + nu + div + "LABEL";
	}

	public String toBlockString() {
		String code = String.format("%-10s", opcode);
		String first = String.format("%-40s", op1.toString());
		String second = String.format("%-40s", op2.toString());
		String third = String.format("%-40s", op3.toString());

		return code + " | " + first + " | " + second + " | " + third + " | " + label;
	}

	public String toStringNorm() {
		if (opcode == OPCODE.NOP) {
			return "nop";
		}

		String x = op1.sr == -1 ? "" : (opcode == OPCODE.LOADI || opcode == OPCODE.OUTPUT ? "" : "r") + op1.sr;
		String y = op2.sr == -1 ? "" : "r" + op2.sr;
		String z = op3.sr == -1 ? "" : "r" + op3.sr;

		String commaCheck = "";
		if (opcode.getOpType() == OPTYPE.ARITHOP) {
			commaCheck = ",";
		}
		String comma = String.format("%-5s", commaCheck);

		String rocketCheck = "";
		if (opcode.getOpType() != OPTYPE.OUTPUT) {
			rocketCheck = "=>";
		}
		String rocket = String.format("%-5s", rocketCheck);

		String code = String.format("%-10s", opcode);
		String first = String.format("%-10s", x);
		String second = String.format("%-10s", y);
		String third = String.format("%-10s", z);

		if (opcode == OPCODE.STORE) {
			return code + first + comma + third + rocket + second;
		}
		return code + first + comma + second + rocket + third;
	}

	public String toPRString() {
		if (opcode == OPCODE.NOP) {
			return "nop";
		}

		String x = op1.pr == -1 ? "" : (opcode == OPCODE.LOADI || opcode == OPCODE.OUTPUT ? "" : "r") + op1.pr;
		String y = op2.pr == -1 ? "" : "r" + op2.pr;
		String z = op3.pr == -1 ? "" : "r" + op3.pr;

		String commaCheck = "";
		if (opcode.getOpType() == OPTYPE.ARITHOP) {
			commaCheck = ",";
		}
		String comma = String.format("%-5s", commaCheck);

		String rocketCheck = "";
		if (opcode.getOpType() != OPTYPE.OUTPUT) {
			rocketCheck = "=>";
		}
		String rocket = String.format("%-5s", rocketCheck);

		String code = String.format("%-10s", opcode);
		String first = String.format("%-10s", x);
		String second = String.format("%-10s", y);
		String third = String.format("%-10s", z);

		if (opcode == OPCODE.STORE) {
			return code + first + comma + third + rocket + second;
		}
		return code + first + comma + second + rocket + third;

	}

	public String toVRString() {
		if (opcode == OPCODE.NOP) {
			return "nop";
		}

		String x = op1.vr == -1 ? "" : (opcode == OPCODE.LOADI || opcode == OPCODE.OUTPUT ? "" : "r") + op1.vr;
		String y = op2.vr == -1 ? "" : "r" + op2.vr;
		String z = op3.vr == -1 ? "" : "r" + op3.vr;

		String commaCheck = "";
		if (opcode.getOpType() == OPTYPE.ARITHOP) {
			commaCheck = ",";
		}
		String comma = String.format("%-5s", commaCheck);

		String rocketCheck = "";
		if (opcode.getOpType() != OPTYPE.OUTPUT) {
			rocketCheck = "=>";
		}
		String rocket = String.format("%-5s", rocketCheck);

		String code = String.format("%-10s", opcode);
		String first = String.format("%-10s", x);
		String second = String.format("%-10s", y);
		String third = String.format("%-10s", z);

		if (opcode == OPCODE.STORE) {
			return code + first + comma + third + rocket + second;
		}
		return code + first + comma + second + rocket + third;

	}

	public String toNodeString() {
		String ret = label + " : " + opcode;

		String x = op1.vr == -1 ? "" : (opcode == OPCODE.LOADI || opcode == OPCODE.OUTPUT ? "" : "r") + op1.vr;
		String y = op2.vr == -1 ? "" : "r" + op2.vr;
		String z = op3.vr == -1 ? "" : "r" + op3.vr;

		switch (opcode.getOpType()) {
		case ARITHOP:
			ret += " " + x + ", " + y + " => " + z;
			break;
		case LOADI:
			ret += " " + op1.pr + " => " + z;
			break;
		case MEMOP:
			if (opcode == OPCODE.LOAD) {
				ret += " " + x + " => " + z;
			} else {
				ret += " " + x + " => " + y;
			}
			break;
		case NOP:
			break;
		case OUTPUT:
			ret += " " + op1.pr;
			break;
		default:
			break;
		}
		return ret;
	}

	public int getTargetRegister() {
		switch (opcode.getOpType()) {
		case ARITHOP:
			return op3.vr;
		case LOADI:
			return op3.vr;
		case MEMOP:
			if (opcode == OPCODE.LOAD) {
				return op3.vr;
			} else {
				return op2.vr;
			}
		case NOP:
			return -1;
		case OUTPUT:
			return -1;
		default:
			return -1;
		}
	}
}
