package ndo;

public class Op {

	final int INFINITY = Integer.MAX_VALUE;

	int sr;
	int vr;
	int pr;
	int nu;
	OPCODE opcode;

	public Op() {
		sr = -1;
		vr = -1;
		pr = -1;
		nu = INFINITY;
	}

	// returns distance to its next use, where index is index in sequence
	public int dist(int index) {
		if (nu == INFINITY) {
			return -1;
		}
		return nu - index;
	}

	public String toString() {
		return (sr == -1 || sr == INFINITY ? "-" : sr) + "\t" + (vr == -1 || vr == INFINITY ? "-" : vr) + "\t"
				+ (pr == -1 || pr == INFINITY ? "-" : pr) + "\t" + (nu == -1 || nu == INFINITY ? "-" : nu);
	}

}
