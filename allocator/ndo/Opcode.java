package ndo;

enum OPCODE {
	LOAD, LOADI, STORE, ADD, SUB, MULT, LSHIFT, RSHIFT, OUTPUT, NOP;

	public OPTYPE getOpType() {
		return (this == ADD || this == SUB || this == MULT || this == LSHIFT || this == RSHIFT) ? OPTYPE.ARITHOP
				: (this == LOAD || this == STORE) ? OPTYPE.MEMOP : (this == LOADI) ? OPTYPE.LOADI : (this == OUTPUT) ? OPTYPE.OUTPUT
						: (this == NOP) ? OPTYPE.NOP : null;
	}

	public String toString() {
		switch (this) {
		case LOAD:
			return "load";
		case ADD:
			return "add";
		case LOADI:
			return "loadI";
		case LSHIFT:
			return "lshift";
		case MULT:
			return "mult";
		case NOP:
			return "nop";
		case OUTPUT:
			return "output";
		case RSHIFT:
			return "rshift";
		case STORE:
			return "store";
		case SUB:
			return "sub";
		default:
			break;
		}
		return "";
	}
}
