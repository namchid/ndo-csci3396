package ndo;

public class Op {

	final int INFINITY = Integer.MAX_VALUE;

	int sr;
	int vr;
	int pr;
	int nu;
	OPCODE opcode;

	public Op() {
		sr = -1;
		vr = -1;
		pr = -1;
		nu = INFINITY;
	}

	// returns distance to its next use, where index is index in sequence
	public int dist(int index) {
		if (nu == INFINITY) {
			return -1;
		}
		return nu - index;
	}

	public String toString() {
		String sourceR = String.format("%-10s", "" + (sr == -1 || sr == INFINITY ? "-" : sr));
		String virtR = String.format("%-10s", "" + (vr == -1 || vr == INFINITY ? "-" : vr));
		String physR = String.format("%-10s", "" + (pr == -1 || pr == INFINITY ? "-" : pr));
		String nextU = String.format("%-10s", "" + (nu == -1 || nu == INFINITY ? "-" : nu));
		
		return sourceR + virtR + physR + nextU;
	}

}
