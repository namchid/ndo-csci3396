productions:
 0: {Goal: [Expr]}
 1: {Expr: [Term, EPrime]}
 2: {EPrime: [PLUS, Term, EPrime]}
 3: {EPrime: [MINUS, Term, EPrime]}
 4: {EPrime: []}
 5: {Term: [Factor, TPrime]}
 6: {TPrime: [TIMES, Factor, TPrime]}
 7: {TPrime: [DIV, Factor, TPrime]}
 8: {TPrime: []}
 9: {Factor: [LP, Expr, RP]}
 10: {Factor: [NUMBER]}
 11: {Factor: [IDENTIFIER]}
firsts:
 Goal: [IDENTIFIER, NUMBER, LP]
 Expr: [IDENTIFIER, LP, NUMBER]
 EPrime: [PLUS, MINUS]
 Term: [IDENTIFIER, NUMBER, LP]
 TPrime: [TIMES, DIV]
 Factor: [LP, NUMBER, IDENTIFIER]
follows:
 Goal: [<EOF>]
 Expr: [<EOF>, RP]
 EPrime: [<EOF>, RP]
 Term: [<EOF>, PLUS, MINUS, RP]
 TPrime: [<EOF>, PLUS, MINUS, RP]
 Factor: [<EOF>, PLUS, MINUS, TIMES, DIV, RP]
nexts:
 0: {Goal: [IDENTIFIER, LP, NUMBER]}
 1: {Expr: [IDENTIFIER, NUMBER, LP]}
 2: {EPrime: [PLUS]}
 3: {EPrime: [MINUS]}
 4: {EPrime: [<EOF>, RP]}
 5: {Term: [LP, NUMBER, IDENTIFIER]}
 6: {TPrime: [TIMES]}
 7: {TPrime: [DIV]}
 8: {TPrime: [<EOF>, PLUS, MINUS, RP]}
 9: {Factor: [LP]}
 10: {Factor: [NUMBER]}
 11: {Factor: [IDENTIFIER]}
