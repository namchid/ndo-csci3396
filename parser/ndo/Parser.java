package ndo;

import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class Parser {

	private String filename = "";

	private String startSymbol = "";
	private static String EOF = "<EOF>";
	private static String EPS = "Epsilon";
	private static String ERR = "--";

	private String thisEps = "";
	private boolean epsSet = false;

	private LinkedHashSet<String> terminals = new LinkedHashSet<String>();
	private LinkedHashSet<String> nonTerminals = new LinkedHashSet<String>();
	private LinkedHashMap<String, HashSet<String>> rhsNonTerminals = new LinkedHashMap<String, HashSet<String>>();

	private HashMap<String, HashSet<Integer>> ntToProductions = new HashMap<String, HashSet<Integer>>();
	private ArrayList<String> productionLhs = new ArrayList<String>();
	private ArrayList<LinkedList<String>> productionsRhs = new ArrayList<LinkedList<String>>();

	private LinkedHashMap<String, LinkedHashSet<String>> first = new LinkedHashMap<String, LinkedHashSet<String>>();
	private LinkedHashMap<String, LinkedHashSet<String>> follow = new LinkedHashMap<String, LinkedHashSet<String>>();
	private LinkedHashMap<Integer, LinkedHashSet<String>> next = new LinkedHashMap<Integer, LinkedHashSet<String>>();

	private LinkedHashMap<String, LinkedHashMap<String, String>> table = new LinkedHashMap<String, LinkedHashMap<String, String>>();

	public Parser(String f) {
		filename = f;

		parse(f);
		cleanRhsNonTerminals();
		computeFirstSets();
		computeFollowSets();
		computeNextSets();
		buildNextTable();
	}

	private void handleError(Exception e, String msg) {
		System.err.print("Well, crap. Real error message: ");
		System.err.println(msg);
		System.exit(1);
	}

	private void parse(String f) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String line = "";
			String nTerm = "";
			String prod = "";
			boolean hasNTerm = false;
			boolean hasStartSymbol = false;
			while ((line = reader.readLine()) != null) {
				try {
					line = line.trim().replace(";", " ;").replace(" +", " ");
					if (line.length() > 0) {
						if (line.startsWith("//")) {
							continue;
						}
						String[] symbols = line.split("[\\s\\t]+");
						for (String s : symbols) {
							if (!hasNTerm) {
								nTerm = s;
								hasNTerm = true;

								if (!hasStartSymbol) {
									startSymbol = s;
									hasStartSymbol = true;
								}
							} else {
								switch (s) {
								case ":":
									break;
								case "|":
									parseProduction(nTerm, prod);
									prod = "";
									break;
								case ";":
									parseProduction(nTerm, prod);
									nTerm = "";
									prod = "";
									hasNTerm = false;
									break;
								default:
									prod += s.trim() + " ";
									break;
								}
							}
						}
					}
				} catch (Exception e) {
					handleError(e, "Trouble parsing line: " + line);
				}
			}
			// case: leftover
			if (!prod.equals("")) {
				parseProduction(nTerm, prod);
			}
			reader.close();
		} catch (Exception e) {
			handleError(e, "Couldn't open file: " + f);
		}
	}

	private void parseProduction(String nTerm, String prod) {
		nTerm = nTerm.trim();
		String[] symbols = prod.trim().split(" +");
		LinkedList<String> thisProd = new LinkedList<String>();

		if (terminals.contains(nTerm)) {
			terminals.remove(nTerm);
		}
		nonTerminals.add(nTerm);

		for (String s : symbols) {

			thisProd.add(s);
			if (!terminals.contains(s) && !nonTerminals.contains(s)) {
				terminals.add(s);
			}

			if (!epsSet) {
				if (s.equals(EPS) || s.equals(EPS.toLowerCase()) || s.equals(EPS.toUpperCase())) {
					thisEps = s;
					epsSet = true;
				}
			}

			HashSet<String> thisRhsNonTerminals = rhsNonTerminals.containsKey(s) ? rhsNonTerminals.get(s) : new HashSet<String>();
			thisRhsNonTerminals.add(nTerm);
			rhsNonTerminals.put(s, thisRhsNonTerminals);
		}

		HashSet<Integer> thisProductions = ntToProductions.containsKey(nTerm) ? ntToProductions.get(nTerm) : new HashSet<Integer>();
		thisProductions.add(productionsRhs.size());
		ntToProductions.put(nTerm, thisProductions);

		productionLhs.add(nTerm);
		productionsRhs.add(thisProd);
	}

	private void cleanRhsNonTerminals() {
		for (String s : rhsNonTerminals.keySet()) {
			HashSet<String> tempSet = new HashSet<String>();
			for (String sym : rhsNonTerminals.get(s)) {
				if (nonTerminals.contains(sym)) {
					tempSet.add(sym);
				}
			}
			rhsNonTerminals.put(s, tempSet);
		}
		terminals.add(EOF);
	}

	private void computeFirstSets() {
		for (String nt : nonTerminals) {
			first.put(nt, new LinkedHashSet<String>());
		}

		for (String t : terminals) {
			LinkedHashSet<String> firstOfT = new LinkedHashSet<String>();
			firstOfT.add(t);
			first.put(t, firstOfT);
		}

		ArrayDeque<String> worklist = new ArrayDeque<String>();
		for (String nt : nonTerminals) {
			worklist.add(nt);
		}

		while (!worklist.isEmpty()) {
			String nt = worklist.poll();
			HashSet<Integer> productions = ntToProductions.get(nt);
			for (int pIndex : productions) {
				LinkedList<String> p = productionsRhs.get(pIndex);

				HashSet<String> rhs = new HashSet<String>(first.get(p.get(0)));

				rhs.remove(thisEps);

				int i = 0;
				int k = p.size() - 1;

				while (i < k && first.get(p.get(i)).contains(thisEps)) {
					i++;
					rhs.addAll(first.get(p.get(i)));
					rhs.remove(thisEps);
				}

				if (i == k && first.get(p.get(k)).contains(thisEps)) {
					rhs.add(thisEps);
				}

				boolean changed = false;
				for (String sym : rhs) {
					if (!first.get(nt).contains(sym)) {
						first.get(nt).add(sym);
						changed = true;
					}
				}

				if (changed) {
					HashSet<String> affectedNonTerminals = rhsNonTerminals.get(nt);
					if (affectedNonTerminals != null) {
						for (String s : affectedNonTerminals) {
							worklist.add(s);
						}
					}

					worklist.add(nt);
				}

			}
		}
	}

	private void computeFollowSets() {
		// TODO
		for (String nt : nonTerminals) {
			follow.put(nt, new LinkedHashSet<String>());
		}

		for (String t : terminals) {
			follow.put(t, new LinkedHashSet<String>());
		}

		follow.get(startSymbol).add(EOF);
		ArrayDeque<String> worklist = new ArrayDeque<String>();
		for (String nt : nonTerminals) {
			worklist.add(nt);
		}

		while (!worklist.isEmpty()) {
			String nt = worklist.poll();
			HashSet<Integer> productions = ntToProductions.get(nt);

			for (int pIndex : productions) {
				LinkedList<String> p = productionsRhs.get(pIndex);

				LinkedHashSet<String> trailer = new LinkedHashSet<String>(follow.get(nt));

				for (int i = p.size() - 1; i >= 0; i--) {
					String bi = p.get(i);
					if (nonTerminals.contains(bi)) {
						boolean changed = false;
						for (String s : trailer) {
							if (!follow.get(bi).contains(s)) {
								changed = true;
								follow.get(bi).add(s);
							}
						}

						if (first.get(bi).contains(thisEps)) {
							trailer.addAll(first.get(bi));
							trailer.remove(thisEps);
						} else {
							trailer = new LinkedHashSet<String>(first.get(bi));
						}

						if (changed) {
							HashSet<String> affectedNonTerminals = rhsNonTerminals.get(nt);
							if (affectedNonTerminals != null) {
								for (String s : affectedNonTerminals) {
									worklist.add(s);
								}
							}

							worklist.add(nt);
						}
					} else {
						trailer = new LinkedHashSet<String>();
						trailer.add(bi);
					}
				}
			}

		}
	}

	private void computeNextSets() {
		for (int i = 0; i < productionLhs.size(); i++) {
			String nt = productionLhs.get(i);
			LinkedList<String> p = productionsRhs.get(i);

			boolean allHaveEps = true;
			LinkedHashSet<String> thisNext = new LinkedHashSet<String>();
			String firstSymNoEps = "";

			for (String bi : p) {
				if (!first.get(bi).contains(thisEps)) {
					allHaveEps = false;
					firstSymNoEps = bi;
					break;
				}
				thisNext.addAll(first.get(bi));
			}

			if (allHaveEps) {
				thisNext.addAll(follow.get(nt));
			} else {
				thisNext = new LinkedHashSet<String>();
				thisNext.addAll(first.get(firstSymNoEps));
			}
			thisNext.remove(thisEps);
			next.put(i, thisNext);
		}

	}

	public void buildNextTable() {
		for (String nt : nonTerminals) {
			LinkedHashMap<String, String> temp = new LinkedHashMap<String, String>();
			table.put(nt, temp);
			for (String t : terminals) {
				if (!t.equals(thisEps)) {
					table.get(nt).put(t, ERR);
				}
			}
		}

		for (int i = 0; i < productionLhs.size(); i++) {
			String nt = productionLhs.get(i);
			LinkedHashSet<String> thisNexts = next.get(i);

			for (String t : thisNexts) {
				if (!table.get(nt).get(t).equals(ERR)) {
					String msg = "Next next conflict for non-terminal <" + nt + "> at terminal <" + t + "> in file <" + filename + ">";
					msg += "\nError caused by productions:\n";
					msg += "  " + productionLhs.get(i) + ": " + productionsRhs.get(i).toString().replaceAll("[\\[,\\]]", "") + "\n  ";
					int otherProduction = Integer.parseInt(table.get(nt).get(t));
					msg += productionLhs.get(otherProduction) + ": " + productionsRhs.get(otherProduction).toString().replaceAll("[\\[,\\]]", "");
					handleError(new Exception(), msg);

				} else {
					table.get(nt).put(t, "" + i);
				}
			}
		}
	}

	public void printTables() {
		printTerminals();
		printNonTerminals();
		printEOF();
		printErrorSymbol();
		printStartSymbol();
		printProductions();
		printTable();
	}

	public void printHumans() {
		printProductions();
		printFirsts();
		printFollows();
		printNexts();
	}

	public void printTerminals() {
		LinkedHashSet<String> temp = new LinkedHashSet<String>(terminals);
		temp.remove(thisEps);
		out.println("terminals: " + temp);
	}

	public void printNonTerminals() {
		out.println("non-terminals: " + nonTerminals);
	}

	public void printEOF() {
		out.println("eof-marker: " + EOF);
	}

	public void printErrorSymbol() {
		out.println("error-marker: " + ERR);
	}

	public void printStartSymbol() {
		out.println("start-symbol: " + startSymbol);
	}

	public void printProductions() {
		out.println("productions:");
		String space = " ";
		for (int i = 0; i < productionsRhs.size(); i++) {
			LinkedList<String> tempRhs = new LinkedList<String>(productionsRhs.get(i));
			tempRhs.remove(thisEps);
			out.println(space + i + ": {" + productionLhs.get(i) + ": " + tempRhs + "}");
		}
	}

	public void printTable() {
		out.println("table:");
		String space = " ";
		for (String nt : table.keySet()) {
			out.println(space + nt + ": " + table.get(nt).toString().replace("=", ": "));
		}
	}

	public void printRhsNonTerminals() {
		out.println("rhs-non-terminals");

		for (String nt : rhsNonTerminals.keySet()) {
			out.println(nt + ": " + rhsNonTerminals.get(nt));
		}
		out.println();
	}

	public void printNtToProductions() {
		out.println("non-terminal-to-production-index:");
		for (String nt : ntToProductions.keySet()) {
			out.println(nt + ": " + ntToProductions.get(nt));
		}
		out.println();
	}

	public void printFirsts() {
		out.println("firsts:");
		for (String s : first.keySet()) {
			if (nonTerminals.contains(s)) {
				LinkedHashSet<String> tempFirsts = new LinkedHashSet<String>(first.get(s));
				tempFirsts.remove(thisEps);
				out.println(" " + s + ": " + tempFirsts);
			}
		}
	}

	public void printFollows() {
		out.println("follows:");
		for (String s : follow.keySet()) {
			if (nonTerminals.contains(s)) {
				out.println(" " + s + ": " + follow.get(s));
			}
		}
	}

	public void printNexts() {
		out.println("nexts:");
		String space = " ";
		for (int i = 0; i < productionsRhs.size(); i++) {
			out.println(space + i + ": {" + productionLhs.get(i) + ": " + next.get(i) + "}");
		}
	}

}
