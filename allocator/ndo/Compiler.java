package ndo;

//import java.io.BufferedWriter;
import java.io.FileInputStream;
//import java.io.FileWriter;
import java.util.LinkedList;
import java.util.Stack;

// working version
public class Compiler {
    
    final int INFINITY = Integer.MAX_VALUE;
    
    private Link root, rover;
    private char c;
    
    private int maxPRNum, maxSRNum, maxVRNum;
    private int VRName;
    private int[] SRtoVR, LastUse, VRtoPR, PRtoVR, PRtoNU, VRtoSpillAd;
    private int reserveReg;
    private int spillAd = 32768;
    
    private LinkedList<Block> blocks = new LinkedList<Block>();
    // private LinkedList<Block> rebuiltBlocks = new LinkedList<Block>();
    private Stack<Integer> freePRStack = new Stack<Integer>();
    private OPCODE[] VRtoOpcode;
    private int[] VRtoValue;
    
    // private String filename = "";
    
    public Compiler(int _maxPR) {
        maxPRNum = _maxPR - 1;
        createStateDiagram();
    }
    
    // ERROR MESSAGE
    private void errorMessage(String msg, Exception e) {
        System.err.println("You made a boo-boo: " + msg);
        e.printStackTrace();
        System.exit(1);
    }
    
    // SET UP FOR DETERMINING OPCODE
    private void createStateDiagram() {
        // end states
        Link eStore = new Link('e', null, null, OPCODE.STORE);
        Link bSub = new Link('b', null, null, OPCODE.SUB);
        Link iLoad = new Link('I', null, null, OPCODE.LOADI);
        Link dLoad = new Link('d', iLoad, null, OPCODE.LOAD);
        Link tLShift = new Link('t', null, null, OPCODE.LSHIFT);
        Link tRShift = new Link('t', null, null, OPCODE.RSHIFT);
        Link tMult = new Link('t', null, null, OPCODE.MULT);
        Link dAdd = new Link('d', null, null, OPCODE.ADD);
        Link pNop = new Link('p', null, null, OPCODE.NOP);
        Link tOutput = new Link('t', null, null, OPCODE.OUTPUT);
        
        // OUTPUT
        Link uOutput = new Link('u', tOutput, null, null);
        Link pOutput = new Link('p', uOutput, null, null);
        Link tOutput2 = new Link('t', pOutput, null, null);
        Link uOutput2 = new Link('u', tOutput2, null, null);
        Link oOutput = new Link('o', uOutput2, null, null);
        
        // NOP
        Link oNop = new Link('o', pNop, null, null);
        Link nNop = new Link('n', oNop, oOutput, null);
        
        // ADD
        Link dAdd2 = new Link('d', dAdd, null, null);
        Link aAdd = new Link('a', dAdd2, nNop, null);
        
        // MULT
        Link lMult = new Link('l', tMult, null, null);
        Link uMult = new Link('u', lMult, null, null);
        Link mMult = new Link('m', uMult, aAdd, null);
        
        // RSHIFT
        Link fRShift = new Link('f', tRShift, null, null);
        Link iRShift = new Link('i', fRShift, null, null);
        Link hRShift = new Link('h', iRShift, null, null);
        Link sRShift = new Link('s', hRShift, null, null);
        Link rRShift = new Link('r', sRShift, mMult, null);
        
        // LSHIFT
        Link fLShift = new Link('f', tLShift, null, null);
        Link iLShift = new Link('i', fLShift, null, null);
        Link hLShift = new Link('h', iLShift, null, null);
        Link sLShift = new Link('s', hLShift, null, null);
        
        // LOAD & LOADI
        Link aLoad = new Link('a', dLoad, null, null);
        Link oLoad = new Link('o', aLoad, sLShift, null);
        Link lLoad = new Link('l', oLoad, rRShift, null);
        
        // SUB
        Link uSub = new Link('u', bSub, null, null);
        
        // STORE
        Link rStore = new Link('r', eStore, null, null);
        Link oStore = new Link('o', rStore, null, null);
        Link tStore = new Link('t', oStore, uSub, null);
        Link sStore = new Link('s', tStore, lLoad, null);
        
        // root
        root = new Link(' ', sStore, null, null);
        rover = root;
    }
    
    // FOR PARSING OPCODE
    private void handleOpcode(char c) {
        try {
            rover = rover.firstChild;
            while (rover != null) {
                if (rover.letter == c) {
                    break;
                } else {
                    rover = rover.nextSibling;
                }
            }
            if (rover == null) {
                errorMessage("Character not in sequence: " + c, new Exception());
            }
        } catch (Exception e) {
            errorMessage("Couldn't handle opcode for character: " + c, e);
        }
    }
    
    // FOR PARSING OPS
    private void handleOps(FileInputStream fis, Block block) {
        Op op1 = new Op();
        Op op2 = new Op();
        Op op3 = new Op();
        try {
            switch (rover.opcode.getOpType()) {
                case ARITHOP:
                    handleArithop(fis, op1, op2, op3);
                    break;
                case LOADI:
                    handleLoadI(fis, op1, op2, op3);
                    break;
                case MEMOP:
                    handleMemop(fis, op1, op2, op3);
                    break;
                case NOP:
                    break;
                case OUTPUT:
                    handleOutput(fis, op1, op2, op3);
                    break;
                default:
                    errorMessage("Somehow the opcode is invalid", new Exception());
                    break;
            }
        } catch (Exception e) {
            errorMessage("Trouble handling ops", e);
        }
        block.op1 = op1;
        block.op2 = op2;
        block.op3 = op3;
        blocks.add(block);
        if (op1.sr > maxSRNum) {
            maxSRNum = op1.sr;
        }
        if (op2.sr > maxSRNum) {
            maxSRNum = op2.sr;
        }
        if (op3.sr > maxSRNum) {
            maxSRNum = op3.sr;
        }
    }
    
    // HANDLES OPCODES: ADD, SUB, LSHIFT, RSHIFT, MULT
    private void handleArithop(FileInputStream fis, Op op1, Op op2, Op op3) {
        try {
            checkRegister();
            c = (char) fis.read();
            op1.sr = readNumber(fis);
            ignoreWhiteSpace(fis);
            checkComma();
            c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRegister();
            c = (char) fis.read();
            op2.sr = readNumber(fis);
            ignoreWhiteSpace(fis);
            checkRocket(fis);
            c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRegister();
            c = (char) fis.read();
            op3.sr = readNumber(fis);
        } catch (Exception e) {
            errorMessage("Problem with arithop sequence", e);
        }
    }
    
    // HANDLES OPCODES: LOADI
    private void handleLoadI(FileInputStream fis, Op op1, Op op2, Op op3) {
        try {
            op1.pr = readNumber(fis);
            // System.out.println(c);
            // c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRocket(fis);
            c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRegister();
            c = (char) fis.read();
            op3.sr = readNumber(fis);
        } catch (Exception e) {
            errorMessage("Problem with loadI sequence", e);
        }
    }
    
    // HANDLES OPCODES: LOAD, STORE
    private void handleMemop(FileInputStream fis, Op op1, Op op2, Op op3) {
        try {
            checkRegister();
            c = (char) fis.read();
            op1.sr = readNumber(fis);
            c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRocket(fis);
            c = (char) fis.read();
            ignoreWhiteSpace(fis);
            checkRegister();
            c = (char) fis.read();
            int temp = readNumber(fis);
            if (rover.opcode == OPCODE.STORE) {
                op2.sr = temp;
            } else if (rover.opcode == OPCODE.LOAD) {
                op3.sr = temp;
            }
        } catch (Exception e) {
            errorMessage("Problem with memop sequence", e);
        }
    }
    
    // HANDLES EACH OPTYPE
    private void handleOutput(FileInputStream fis, Op op1, Op op2, Op op3) {
        try {
            op1.pr = readNumber(fis);
        } catch (Exception e) {
            errorMessage("Problem with output sequence", e);
        }
    }
    
    // READS REGISTER NUMBER OR CONSTANT
    private int readNumber(FileInputStream fis) {
        int reg = 0;
        try {
            while (c != ' ' && c != ',' && c != '\t' && c != '\n') {
                if (Character.isDigit(c)) {
                    reg = reg * 10 + Character.digit(c, 10);
                } else if (c == '=') {
                    break;
                } else if (c != ' ' && c != ',' && c != '\t') {
                    // System.out.println("c is: " + c);
                    // System.out.println((char) fis.read());
                    errorMessage("Invalid register number", new Exception());
                }
                c = (char) fis.read();
            }
            return reg;
        } catch (Exception e) {
            errorMessage("Trouble parsing number", e);
        }
        return -1;
    }
    
    // CHECKS FOR INDICATION OF REGISTER
    private void checkRegister() {
        if (c != 'r') {
            errorMessage("Invalid. Expecting a register.", new Exception());
        }
    }
    
    // CHECKS FOR COMMA WHEN PARSING
    private void checkComma() {
        if (c != ',') {
            errorMessage("Expecting comma between registers", new Exception());
        }
    }
    
    // CHECKS FOR =>
    private void checkRocket(FileInputStream fis) {
        try {
            if (c != '=') {
                errorMessage("Expecting a rocket (=>)", new Exception());
            }
            c = (char) fis.read();
            if (c != '>') {
                errorMessage("Expecting a rocket (=>)", new Exception());
            }
        } catch (Exception e) {
            errorMessage("Problem when checking for rocket", new Exception());
        }
    }
    
    // MOVES c TO NEXT NON-WHITESPACE
    private void ignoreWhiteSpace(FileInputStream fis) {
        try {
            while (c == ' ' || c == '\t') {
                c = (char) fis.read();
            }
        } catch (Exception e) {
            errorMessage("Error while reading whitespace", e);
        }
    }
    
    // MOVES c TO NEXT LINE DUE TO COMMENT
    private void ignoreComment(FileInputStream fis) {
        try {
            c = (char) fis.read();
            if (c == '/') {
                while (c != '\n') {
                    c = (char) fis.read();
                }
            }
        } catch (Exception e) {
            errorMessage("Error while reading comment", e);
        }
    }
    
    // SCANS, PARSES, STORES IN BLOCKS
    public void scanParse(String infile) {
        // filename = infile;
        
        c = ' ';
        try {
            FileInputStream fis = new FileInputStream(infile);
            Block tempBlock = new Block();
            
            while (fis.available() > 0) {
                c = (char) fis.read();
                ignoreWhiteSpace(fis);
                if (c == '/') {
                    ignoreComment(fis);
                } else {
                    while (c != '\n') {
                        if (c == '/') {
                            ignoreComment(fis);
                        } else if (c != '\t' && c != ' ') {
                            handleOpcode(c);
                            c = (char) fis.read();
                        } else {
                            if (rover.opcode == null) {
                                errorMessage("Invalid opcode", new Exception());
                            }
                            tempBlock.opcode = rover.opcode;
                            ignoreWhiteSpace(fis);
                            handleOps(fis, tempBlock);
                            ignoreWhiteSpace(fis);
                            if (c == '\n') {
                                break;
                            } else if (c != '/') {
                                errorMessage("Invalid syntax", new Exception());
                            }
                        }
                    }
                }
                rover = root;
                tempBlock = new Block();
            }
            maxSRNum += 1;
            fis.close();
        } catch (Exception e) {
            errorMessage("Error reading file: " + infile, e);
        }
    }
    
    // COMPUTES LAST USE OF EACH SOURCE REGISTER
    public void computeLastUse() {
        VRName = 0;
        maxVRNum = 0;
        
        SRtoVR = new int[maxSRNum];
        LastUse = new int[maxSRNum];
        
        for (int i = 0; i < maxSRNum; i++) {
            SRtoVR[i] = -1;
            LastUse[i] = INFINITY;
        }
        
        for (int i = blocks.size() - 1; i >= 0; i--) {
            Op tempBlock = blocks.get(i).op3;
            update(tempBlock, i);
            if (tempBlock.sr != -1) {
                SRtoVR[tempBlock.sr] = -1;
                LastUse[tempBlock.sr] = INFINITY;
            }
            update(blocks.get(i).op1, i);
            update(blocks.get(i).op2, i);
        }
        maxVRNum++;
    }
    
    // HELPER FOR computeLastUse(): UPDATES LAST USE
    private void update(Op op, int index) {
        if (op.sr == -1) {
            return;
        }
        
        if (SRtoVR[op.sr] == -1) {
            SRtoVR[op.sr] = VRName++;
        }
        
        op.vr = SRtoVR[op.sr];
        op.nu = LastUse[op.sr];
        LastUse[op.sr] = index;
        if (op.vr > maxVRNum) {
            maxVRNum = op.vr;
        }
    }
    
    // ALLOCATES PHYSICAL REGISTERS
    public void allocate() {
        VRtoPR = new int[maxVRNum];
        PRtoVR = new int[maxPRNum];
        PRtoNU = new int[maxPRNum];
        VRtoSpillAd = new int[maxVRNum];
        reserveReg = maxPRNum;
        
        VRtoOpcode = new OPCODE[maxVRNum];
        VRtoValue = new int[maxVRNum];
        
        for (int i = 0; i < maxVRNum; i++) {
            VRtoPR[i] = -1;
            VRtoSpillAd[i] = -1;
            VRtoValue[i] = -1000;
        }
        
        for (int i = 0; i < maxPRNum; i++) {
            PRtoVR[i] = -1;
        }
        
        for (int i = maxPRNum - 1; i >= 0; i--) {
            freePRStack.push(i);
            PRtoNU[i] = -1;
        }
        
        for (Block block : blocks) {
            int rx = ensure(block.op1);
            int ry = ensure(block.op2);
            
            if (rx != -1 && block.op1.nu == INFINITY) {
                freePRStack.add(rx);
                int temp = VRtoPR[block.op1.vr];
                VRtoPR[block.op1.vr] = -1;
                PRtoVR[temp] = -1;
            }
            
            if (ry != -1 && block.op2.nu == INFINITY) {
                freePRStack.add(ry);
                int temp = VRtoPR[block.op2.vr];
                VRtoPR[block.op2.vr] = -1;
                PRtoVR[temp] = -1;
            }
            
            if (block.opcode != OPCODE.STORE && block.opcode != OPCODE.OUTPUT && block.opcode != OPCODE.NOP) {
                allocateReg(block.op3);
            }
            
            if (block.op1.vr != -1 && block.op1.nu != INFINITY) {
                PRtoNU[block.op1.pr] = block.op1.nu;
            }
            if (block.op2.vr != -1 && block.op2.nu != INFINITY) {
                PRtoNU[block.op2.pr] = block.op2.nu;
            }
            if (block.op3.vr != -1) {
                PRtoNU[block.op3.pr] = block.op3.nu;
            }
            
            if (block.op3.vr != -1) {
                VRtoOpcode[block.op3.vr] = block.opcode;
            }
            if (block.opcode == OPCODE.LOADI) {
                VRtoValue[block.op3.vr] = block.op1.pr;
            }
            
            // rebuiltBlocks.add(block);
            System.out.println(block.toPRString());
        }
    }
    
    // RETURNS PR, RESTORES IF NEEDED
    private int ensure(Op op) {
        if (op.vr != -1) {
            if (VRtoPR[op.vr] != -1) {
                op.pr = VRtoPR[op.vr];
                return op.pr;
            } else {
                int result = allocateReg(op);
                int restoredVR = PRtoVR[result];
                int restoredAddress = VRtoSpillAd[restoredVR];
                
                OPCODE restoredOpcode = VRtoOpcode[restoredVR];
                
                if (restoredOpcode == OPCODE.LOADI) {
                    Op op1 = new Op();
                    op1.pr = VRtoValue[restoredVR];
                    Op op2 = new Op();
                    Op op3 = new Op();
                    op3.pr = result;
                    Block block1 = new Block(OPCODE.LOADI, op1, op2, op3);
                    // rebuiltBlocks.add(block1);
                    
                    System.out.println(block1.toPRString());
                } else {
                    Op op1 = new Op();
                    op1.pr = restoredAddress;
                    Op op2 = new Op();
                    Op op3 = new Op();
                    op3.pr = reserveReg;
                    Block block1 = new Block(OPCODE.LOADI, op1, op2, op3);
                    // rebuiltBlocks.add(block1);
                    
                    Op op11 = new Op();
                    op11.pr = reserveReg;
                    Op op22 = new Op();
                    Op op33 = new Op();
                    op33.pr = result;
                    Block block11 = new Block(OPCODE.LOAD, op11, op22, op33);
                    // rebuiltBlocks.add(block11);
                    
                    System.out.println(block1.toPRString());
                    System.out.println(block11.toPRString());
                }
                
                return result;
            }
        }
        return -1;
    }
    
    // GETS A FREE PR, SPILLS IF NEEDED
    private int allocateReg(Op op) {
        int pr = -1;
        if (!freePRStack.isEmpty()) {
            pr = freePRStack.pop();
        } else {
            int prWithMaxNU = 0;
            for (int i = 1; i < PRtoNU.length; i++) {
                if (PRtoNU[i] > PRtoNU[prWithMaxNU]) {
                    prWithMaxNU = i;
                }
                if (PRtoNU[i] == PRtoNU[prWithMaxNU]) {
                    OPCODE currPR = VRtoOpcode[PRtoVR[prWithMaxNU]];
                    OPCODE thisPR = VRtoOpcode[PRtoVR[i]];
                    
                    if (currPR != OPCODE.LOADI) {
                        if (thisPR == OPCODE.LOADI) {
                            prWithMaxNU = i;
                        }
                    }
                }
            }
            pr = prWithMaxNU;
            
            int spilledVR = PRtoVR[pr];
            
            OPCODE spilledOpcode = VRtoOpcode[spilledVR];
            VRtoPR[spilledVR] = -1;
            
            if (VRtoSpillAd[spilledVR] == -1) {
                if (spilledOpcode != OPCODE.LOADI) {
                    VRtoSpillAd[spilledVR] = spillAd;
                    spillAd += 4;
                    
                    Op op1 = new Op();
                    op1.pr = (spillAd - 4);
                    Op op2 = new Op();
                    Op op3 = new Op();
                    op3.pr = reserveReg;
                    Block block1 = new Block(OPCODE.LOADI, op1, op2, op3);
                    // rebuiltBlocks.add(block1);
                    
                    Op op11 = new Op();
                    op11.pr = pr;
                    Op op22 = new Op();
                    op22.pr = reserveReg;
                    Op op33 = new Op();
                    Block block11 = new Block(OPCODE.STORE, op11, op22, op33);
                    // rebuiltBlocks.add(block11);
                    
                    System.out.println(block1.toPRString());
                    System.out.println(block11.toPRString());
                }
            }
        }
        
        PRtoNU[pr] = -1;
        PRtoVR[pr] = op.vr;
        VRtoPR[op.vr] = pr;
        op.pr = pr;
        
        return pr;
    }
    
    // PRINTS PR STRINGS OF REBUILT BLOCKS
    // public void printPRStringsRebuilt() {
    // for (Block b : rebuiltBlocks) {
    // System.out.println(b.toPRString());
    // }
    // }
    
    // PRINTS BLOCKS IN INPUT FORMAT
    public void printBlocksNorm() {
        for (Block b : blocks) {
            System.out.println(b.toStringNorm());
        }
        System.out.println();
    }
    
    // PRINTS BLOCK TABLES WITH FULL INFO
    public void printBlocks() {
        if (blocks.isEmpty())
            return;
        System.out.println(blocks.get(0).headerString());
        for (Block b : blocks) {
            System.out.println(b.toString());
        }
        System.out.println();
    }
    
    // PRINTS THE PR STRING
    public void printPRStrings() {
        for (Block b : blocks) {
            System.out.println(b.toPRString());
        }
    }
    
    // WRITE FINAL PR OUT TO FILE
    // public void writePRtoFile(String outfile) {
    // try {
    // BufferedWriter writer = new BufferedWriter(new FileWriter(outfile));
    // for (Block b : rebuiltBlocks) {
    // writer.append(b.toPRString() + "\n");
    // }
    // writer.flush();
    // writer.close();
    // } catch (Exception e) {
    // errorMessage("Problem writing out to file", e);
    // }
    // }
    
    // public void writePRtoFile() {
    // String outfile = "res_";
    // if (filename.contains("/")) {
    // String[] ss = filename.split("/");
    // outfile += ss[ss.length - 1];
    // } else {
    // outfile += filename;
    // }
    //
    // writePRtoFile(outfile);
    // }
    
    public void printAllTables() {
        System.out.print("VRtoPR: {");
        for (int i = 0; i < VRtoPR.length; i++) {
            System.out.print(i + ": " + VRtoPR[i] + ", ");
            if (i % 10 == 0 && i > 0) {
                System.out.println();
            }
        }
        System.out.println("}");
        
        System.out.print("PRtoVR: {");
        for (int i = 0; i < PRtoVR.length; i++) {
            System.out.print(i + ": " + PRtoVR[i] + ", ");
            if (i % 10 == 0 && i > 0) {
                System.out.println();
            }
        }
        System.out.println("}");
        
        System.out.print("PRtoNU: {");
        for (int i = 0; i < PRtoNU.length; i++) {
            System.out.print(i + ": " + PRtoNU[i] + ", ");
            if (i % 10 == 0 && i > 0) {
                System.out.println();
            }
        }
        System.out.println("}");
        
        System.out.print("VRtoSpillAd: {");
        for (int i = 0; i < VRtoSpillAd.length; i++) {
            System.out.print(i + ": " + VRtoSpillAd[i] + ", ");
            if (i % 10 == 0 && i > 0) {
                System.out.println();
            }
        }
        System.out.println("}");
        
        System.out.println("----------");
    }
    
    static void printUsage() {
        System.err.println("Usage: ./alloc -k <max_pr_registers> <filename>");
        System.err.println("Usage: ./alloc -h");
    }
    
    public static void main(String[] args) {
        boolean kFlag = false;
        boolean nFlag = false;
        int num = -1;
        for (String arg : args) {
            if (arg.equals("-h")) {
                printUsage();
                System.exit(0);
            } else if (kFlag) {
                if (!nFlag) {
                    try {
                        num = Integer.parseInt(arg);
                        nFlag = true;
                    } catch (Exception e) {
                        System.err.println("Invalid usage." + arg);
                        printUsage();
                        e.printStackTrace();
                        System.exit(1);
                    }
                } else {
                    Compiler compiler = new Compiler(num);
                    compiler.scanParse(arg);
                    compiler.computeLastUse();
                    compiler.allocate();
                    // compiler.printPRStringsRebuilt();
                }
            } else if (arg.equals("-k")) {
                kFlag = true;
            } else {
                System.out.println("Invalid usage.");
                printUsage();
                System.exit(1);
            }
        }
    }
    
}
