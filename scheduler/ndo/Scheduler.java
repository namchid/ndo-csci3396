package ndo;

import java.io.FileInputStream;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class Scheduler {

	final int INFINITY = Integer.MAX_VALUE;

	private Link root, rover;
	private char c;

	private int maxSRNum, maxVRNum;
	private int VRName;
	private int[] SRtoVR, LastUse;

	private LinkedList<Block> blocks = new LinkedList<Block>();

	private String filename;
	private int label = 1;

	private LinkedHashMap<String, LinkedHashSet<String>> edges = new LinkedHashMap<String, LinkedHashSet<String>>();
	private LinkedHashMap<String, Integer> weights = new LinkedHashMap<String, Integer>();
	private LinkedHashMap<String, OPCODE> labelToOpcode = new LinkedHashMap<String, OPCODE>();

	public Scheduler(String filename) {
		this.filename = filename;
		createStateDiagram();
		scanParse();
		computeLastUse();
		computeEdges();
		computeLatency();
	}

	// ERROR MESSAGE
	private void errorMessage(String msg, Exception e) {
		System.err.println("You made a boo-boo in file: " + filename + ", " + rover.opcode);
		System.err.println(msg);
		e.printStackTrace();
		System.exit(1);
	}

	// SET UP FOR DETERMINING OPCODE
	private void createStateDiagram() {
		// end states
		Link eStore = new Link('e', null, null, OPCODE.STORE);
		Link bSub = new Link('b', null, null, OPCODE.SUB);
		Link iLoad = new Link('I', null, null, OPCODE.LOADI);
		Link dLoad = new Link('d', iLoad, null, OPCODE.LOAD);
		Link tLShift = new Link('t', null, null, OPCODE.LSHIFT);
		Link tRShift = new Link('t', null, null, OPCODE.RSHIFT);
		Link tMult = new Link('t', null, null, OPCODE.MULT);
		Link dAdd = new Link('d', null, null, OPCODE.ADD);
		Link pNop = new Link('p', null, null, OPCODE.NOP);
		Link tOutput = new Link('t', null, null, OPCODE.OUTPUT);

		// OUTPUT
		Link uOutput = new Link('u', tOutput, null, null);
		Link pOutput = new Link('p', uOutput, null, null);
		Link tOutput2 = new Link('t', pOutput, null, null);
		Link uOutput2 = new Link('u', tOutput2, null, null);
		Link oOutput = new Link('o', uOutput2, null, null);

		// NOP
		Link oNop = new Link('o', pNop, null, null);
		Link nNop = new Link('n', oNop, oOutput, null);

		// ADD
		Link dAdd2 = new Link('d', dAdd, null, null);
		Link aAdd = new Link('a', dAdd2, nNop, null);

		// MULT
		Link lMult = new Link('l', tMult, null, null);
		Link uMult = new Link('u', lMult, null, null);
		Link mMult = new Link('m', uMult, aAdd, null);

		// RSHIFT
		Link fRShift = new Link('f', tRShift, null, null);
		Link iRShift = new Link('i', fRShift, null, null);
		Link hRShift = new Link('h', iRShift, null, null);
		Link sRShift = new Link('s', hRShift, null, null);
		Link rRShift = new Link('r', sRShift, mMult, null);

		// LSHIFT
		Link fLShift = new Link('f', tLShift, null, null);
		Link iLShift = new Link('i', fLShift, null, null);
		Link hLShift = new Link('h', iLShift, null, null);
		Link sLShift = new Link('s', hLShift, null, null);

		// LOAD & LOADI
		Link aLoad = new Link('a', dLoad, null, null);
		Link oLoad = new Link('o', aLoad, sLShift, null);
		Link lLoad = new Link('l', oLoad, rRShift, null);

		// SUB
		Link uSub = new Link('u', bSub, null, null);

		// STORE
		Link rStore = new Link('r', eStore, null, null);
		Link oStore = new Link('o', rStore, null, null);
		Link tStore = new Link('t', oStore, uSub, null);
		Link sStore = new Link('s', tStore, lLoad, null);

		// root
		root = new Link(' ', sStore, null, null);
		rover = root;
	}

	// FOR PARSING OPCODE
	private void handleOpcode(char c) {
		try {
			rover = rover.firstChild;
			while (rover != null) {
				if (rover.letter == c) {
					break;
				} else {
					rover = rover.nextSibling;
				}
			}
			if (rover == null) {
				errorMessage("Character not in sequence: " + c, new Exception());
			}
		} catch (Exception e) {
			errorMessage("Couldn't handle opcode for character: " + c, e);
		}
	}

	// FOR PARSING OPS
	private void handleOps(FileInputStream fis, Block block) {
		Op op1 = new Op();
		Op op2 = new Op();
		Op op3 = new Op();
		try {
			switch (rover.opcode.getOpType()) {
			case ARITHOP:
				handleArithop(fis, op1, op2, op3);
				break;
			case LOADI:
				handleLoadI(fis, op1, op2, op3);
				break;
			case MEMOP:
				handleMemop(fis, op1, op2, op3);
				break;
			case NOP:
				break;
			case OUTPUT:
				handleOutput(fis, op1, op2, op3);
				break;
			default:
				errorMessage("Somehow the opcode is invalid", new Exception());
				break;
			}
		} catch (Exception e) {
			errorMessage("Trouble handling ops", e);
		}
		block.op1 = op1;
		block.op2 = op2;
		block.op3 = op3;
		block.label = "l" + label;
		label++;
		blocks.add(block);
		if (op1.sr > maxSRNum) {
			maxSRNum = op1.sr;
		}
		if (op2.sr > maxSRNum) {
			maxSRNum = op2.sr;
		}
		if (op3.sr > maxSRNum) {
			maxSRNum = op3.sr;
		}
	}

	// HANDLES OPCODES: ADD, SUB, LSHIFT, RSHIFT, MULT
	private void handleArithop(FileInputStream fis, Op op1, Op op2, Op op3) {
		try {
			// sSystem.out.println("here");

			checkRegister();
			c = (char) fis.read();
			op1.sr = readNumber(fis);
			ignoreWhiteSpace(fis);
			checkComma();
			c = (char) fis.read();
			ignoreWhiteSpace(fis);
			checkRegister();
			c = (char) fis.read();
			op2.sr = readNumber(fis);
			ignoreWhiteSpace(fis);
			checkRocket(fis);
			c = (char) fis.read();
			ignoreWhiteSpace(fis);
			checkRegister();
			c = (char) fis.read();
			op3.sr = readNumber(fis);
		} catch (Exception e) {
			errorMessage("Problem with arithop sequence", e);
		}
	}

	// HANDLES OPCODES: LOADI
	private void handleLoadI(FileInputStream fis, Op op1, Op op2, Op op3) {
		try {
			op1.pr = readNumber(fis);
			// System.out.println(c);
			// c = (char) fis.read();
			ignoreWhiteSpace(fis);
			checkRocket(fis);
			c = (char) fis.read();
			ignoreWhiteSpace(fis);
			checkRegister();
			c = (char) fis.read();
			op3.sr = readNumber(fis);
		} catch (Exception e) {
			errorMessage("Problem with loadI sequence", e);
		}
	}

	// HANDLES OPCODES: LOAD, STORE
	private void handleMemop(FileInputStream fis, Op op1, Op op2, Op op3) {
		try {
			checkRegister();
			c = (char) fis.read();
			op1.sr = readNumber(fis);
			ignoreWhiteSpace(fis);
			// c = (char) fis.read();
			checkRocket(fis);
			c = (char) fis.read();
			ignoreWhiteSpace(fis);
			checkRegister();
			c = (char) fis.read();
			int temp = readNumber(fis);
			if (rover.opcode == OPCODE.STORE) {
				op2.sr = temp;
			} else if (rover.opcode == OPCODE.LOAD) {
				op3.sr = temp;
			}
		} catch (Exception e) {
			errorMessage("Problem with memop sequence", e);
		}
	}

	// HANDLES EACH OPTYPE
	private void handleOutput(FileInputStream fis, Op op1, Op op2, Op op3) {
		try {
			op1.pr = readNumber(fis);
		} catch (Exception e) {
			errorMessage("Problem with output sequence", e);
		}
	}

	// READS REGISTER NUMBER OR CONSTANT
	private int readNumber(FileInputStream fis) {
		int reg = 0;
		try {
			while (fis.available() > 0 && c != ' ' && c != ',' && c != '\t' && c != '\n' && c != '/') {
				if (Character.isDigit(c)) {
					reg = reg * 10 + Character.digit(c, 10);
				} else if (c == '=') {
					break;
				} else if (c != ' ' && c != ',' && c != '\t') {
					// System.out.println("c is: " + c);
					// System.out.println((char) fis.read());
					errorMessage("Invalid register number", new Exception());
				}
				c = (char) fis.read();
			}
			return reg;
		} catch (Exception e) {
			errorMessage("Trouble parsing number", e);
		}
		return -1;
	}

	// CHECKS FOR INDICATION OF REGISTER
	private void checkRegister() {
		if (c != 'r') {
			errorMessage("Invalid. Expecting a register.", new Exception());
		}
	}

	// CHECKS FOR COMMA WHEN PARSING
	private void checkComma() {
		if (c != ',') {
			errorMessage("Expecting comma between registers", new Exception());
		}
	}

	// CHECKS FOR =>
	private void checkRocket(FileInputStream fis) {
		try {
			if (c != '=') {
				errorMessage("Expecting a rocket (=>)", new Exception());
			}
			c = (char) fis.read();
			if (c != '>') {
				errorMessage("Expecting a rocket (=>)", new Exception());
			}
		} catch (Exception e) {
			errorMessage("Problem when checking for rocket", new Exception());
		}
	}

	// MOVES c TO NEXT NON-WHITESPACE
	private void ignoreWhiteSpace(FileInputStream fis) {
		try {
			while (fis.available() > 0 && (c == ' ' || c == '\t')) {
				c = (char) fis.read();
			}
		} catch (Exception e) {
			errorMessage("Error while reading whitespace", e);
		}
	}

	// MOVES c TO NEXT LINE DUE TO COMMENT
	private void ignoreComment(FileInputStream fis) {
		try {
			c = (char) fis.read();
			if (c == '/') {
				while (fis.available() > 0 && c != '\n') {
					c = (char) fis.read();
				}
			}
		} catch (Exception e) {
			errorMessage("Error while reading comment", e);
		}
	}

	// SCANS, PARSES, STORES IN BLOCKS
	public void scanParse() {
		c = ' ';
		try {
			FileInputStream fis = new FileInputStream(filename);
			Block tempBlock = new Block();

			while (fis.available() > 0) {
				c = (char) fis.read();
				ignoreWhiteSpace(fis);
				if (c == '/') {
					ignoreComment(fis);
				} else {
					while (c != '\n' && fis.available() > 0) {
						if (c == '/') {
							ignoreComment(fis);
						} else if (c != '\t' && c != ' ') {
							handleOpcode(c);
							c = (char) fis.read();
						} else {
							if (rover.opcode == null) {
								errorMessage("Invalid opcode", new Exception());
							}
							// System.out.println("opcode = " +
							// tempBlock.opcode);

							tempBlock.opcode = rover.opcode;
							ignoreWhiteSpace(fis);
							handleOps(fis, tempBlock);
							ignoreWhiteSpace(fis);

							if (c == '\n') {
								break;
							} else if (c != '/' && fis.available() > 0) {
								errorMessage("Invalid syntax", new Exception());
							}
						}
					}
				}
				if (rover.opcode == OPCODE.NOP) {
					tempBlock.opcode = rover.opcode;
					handleOps(fis, tempBlock);
				}
				rover = root;
				tempBlock = new Block();
			}
			maxSRNum += 1;
			fis.close();
		} catch (Exception e) {
			errorMessage("Error reading file: " + filename, e);
		}
	}

	// COMPUTES LAST USE OF EACH SOURCE REGISTER
	private void computeLastUse() {
		VRName = 0;
		maxVRNum = 0;

		SRtoVR = new int[maxSRNum];
		LastUse = new int[maxSRNum];

		for (int i = 0; i < maxSRNum; i++) {
			SRtoVR[i] = -1;
			LastUse[i] = INFINITY;
		}

		for (int i = blocks.size() - 1; i >= 0; i--) {
			Op tempBlock = blocks.get(i).op3;
			update(tempBlock, i);
			if (tempBlock.sr != -1) {
				SRtoVR[tempBlock.sr] = -1;
				LastUse[tempBlock.sr] = INFINITY;
			}
			update(blocks.get(i).op1, i);
			update(blocks.get(i).op2, i);
		}
		maxVRNum++;
	}

	// HELPER FOR computeLastUse(): UPDATES LAST USE
	private void update(Op op, int index) {
		if (op.sr == -1) {
			return;
		}

		if (SRtoVR[op.sr] == -1) {
			SRtoVR[op.sr] = VRName++;
		}

		op.vr = SRtoVR[op.sr];
		op.nu = LastUse[op.sr];
		LastUse[op.sr] = index;
		if (op.vr > maxVRNum) {
			maxVRNum = op.vr;
		}
	}

	// CREATES DEPENDENCIES GRAPH / EDGES HASHMAP
	private void computeEdges() {
		// LOAD, LOADI, STORE, ADD, SUB, MULT, LSHIFT, RSHIFT, OUTPUT, NOP;

		LinkedHashSet<String> prevLoadInstructions = new LinkedHashSet<String>();
		String prevOutputInstruction = null;
		String prevStoreInstruction = null;

		HashMap<Integer, String> registerToLabel = new HashMap<Integer, String>();

		for (Block b : blocks) {
			OPCODE opcode = b.opcode;
			String label = b.label;
			LinkedHashSet<String> tempParents = new LinkedHashSet<String>();

			// TODO
			int vr1 = -1, vr2 = -1, vr3 = -1;
			switch (opcode.getOpType()) {
			case ARITHOP: // ADD, SUB, MULT, LSHIFT, RSHIFT
				vr1 = b.op1.vr;
				vr2 = b.op2.vr;
				vr3 = b.op3.vr;

				if (registerToLabel.containsKey(vr1)) {
					tempParents.add(registerToLabel.get(vr1));
				}
				if (registerToLabel.containsKey(vr2)) {
					tempParents.add(registerToLabel.get(vr2));
				}

				registerToLabel.put(vr3, label);
				break;
			case LOADI:
				vr3 = b.op3.vr;
				registerToLabel.put(vr3, label);
				break;
			case MEMOP: // LOAD or STORE
				if (prevStoreInstruction != null) {
					tempParents.add(prevStoreInstruction);
				}

				if (opcode == OPCODE.STORE) {
					vr2 = b.op2.vr;
					vr1 = b.op1.vr;

					prevStoreInstruction = label;
					if (registerToLabel.containsKey(vr1)) {
						tempParents.add(registerToLabel.get(vr1));
					}

					if (registerToLabel.containsKey(vr2)) {
						tempParents.add(registerToLabel.get(vr2));
					}

					if (prevOutputInstruction != null) {
						tempParents.add(prevOutputInstruction);
					}

					for (String pl : prevLoadInstructions) {
						tempParents.add(pl);
					}
					registerToLabel.put(vr2, label);
				} else if (opcode == OPCODE.LOAD) {
					vr3 = b.op3.vr;
					vr1 = b.op1.vr;

					tempParents.add(registerToLabel.get(vr1));
					prevLoadInstructions.add(label);
					registerToLabel.put(vr3, label);
				}
				break;
			case NOP:
				break;
			case OUTPUT:
				if (prevOutputInstruction != null) {
					tempParents.add(prevOutputInstruction);
				}
				if (prevStoreInstruction != null) {
					tempParents.add(prevStoreInstruction);
				}
				prevOutputInstruction = label;
				break;
			default:
				break;

			}

			edges.put(label, tempParents);
			labelToOpcode.put(label, b.opcode);
		}
	}

	// COMPUTES THE LATENCY WEIGHTS
	private void computeLatency() {
		LinkedHashMap<String, LinkedHashSet<String>> revEdges = new LinkedHashMap<String, LinkedHashSet<String>>();
		ArrayDeque<String> worklist = new ArrayDeque<String>();

		LinkedHashSet<String> tempNoParents = new LinkedHashSet<String>();

		for (String depender : edges.keySet()) {
			tempNoParents.add(depender);
			weights.put(depender, 0);
			for (String dependency : edges.get(depender)) {
				LinkedHashSet<String> temp = revEdges.containsKey(dependency) ? revEdges.get(dependency) : new LinkedHashSet<String>();
				temp.add(depender);
				revEdges.put(dependency, temp);

				// TODO: check might be redundant
				// if (tempNoParents.contains(dependency)) {
				tempNoParents.remove(dependency);
				// }
			}
		}

		for (String label : tempNoParents) {
			worklist.add(label);
			weights.put(label, getLatency(label));
			// revEdges.put(label, fakeLabel);
		}

		// System.out.println(worklist);

		while (!worklist.isEmpty()) {
			String topLabel = worklist.poll();
			int topWeight = weights.get(topLabel);

			// System.out.println("top: " + topLabel);

			for (String parent : edges.get(topLabel)) {
				revEdges.get(parent).remove(topLabel);
				weights.put(parent, Math.max(weights.get(parent), topWeight));
				if (revEdges.get(parent).size() == 0) {
					revEdges.remove(parent);
					worklist.add(parent);
					weights.put(parent, weights.get(parent) + getLatency(parent));
				}
			}

		}

		// System.out.println("no parents: " + tempNoParents);
		// System.out.println("revEdges: " + revEdges);
		// System.out.println("weights: " + weights);
		// System.out.println("worklist: " + worklist);

	}

	private int getLatency(String label) {
		// if (label.equals("LOL")) {
		// return 0;
		// }

		switch (labelToOpcode.get(label)) {
		case LOAD:
			return 3;
		case STORE:
			return 3;
		case MULT:
			return 2;
		default:
			return 1;
		}
	}

	// PRINTS BLOCK TABLE
	public void printTable() {
		if (blocks.isEmpty())
			return;
		System.out.println(blocks.get(0).headerString());
		for (Block b : blocks) {
			System.out.println(b.toBlockString());
		}
		System.out.println();
	}

	// PRINT VRs
	public void printVRStrings() {
		for (Block b : blocks) {
			System.out.println(b.toVRString());
		}
	}

	// PRINT NODES
	public void printNodes() {
		System.out.println("nodes: ");
		for (Block b : blocks) {
			System.out.println("\t" + b.toNodeString());
		}
	}

	// PRINT EDGES
	public void printEdges() {
		System.out.println("edges: ");
		for (String label : edges.keySet()) {
			String es = edges.get(label).size() > 0 ? edges.get(label).toString().replace("[", "{ ").replace("]", " }") : "{ }";
			System.out.println("\t" + label + " : " + es);
		}
	}

	// PRINTS LABEL: OPCODES
	public void printLabelToOpcodes() {
		for (String label : labelToOpcode.keySet()) {
			System.out.println("\t" + label + " : " + labelToOpcode.get(label));
		}
	}

	// PRINTS WEIGHTS
	public void printWeights() {
		System.out.println("weights: ");
		for (String label : weights.keySet()) {
			System.out.println("\t" + label + " : " + weights.get(label));
		}
	}

}
