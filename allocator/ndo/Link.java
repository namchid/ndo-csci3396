package ndo;

public class Link {

	char letter;
	Link firstChild;
	Link nextSibling;
	OPCODE opcode;

	public Link(char letter, Link firstChild, Link nextSibling, OPCODE opcode) {
		this.letter = letter;
		this.firstChild = firstChild;
		this.nextSibling = nextSibling;
		this.opcode = opcode;
	}

}
