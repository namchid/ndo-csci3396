package ndo;

public class Block {

	OPCODE opcode;
	Op op1;
	Op op2;
	Op op3;
	final int INFINITY = Integer.MAX_VALUE;

	public Block(OPCODE opcode, Op op1, Op op2, Op op3) {
		this.opcode = opcode;
		this.op1 = op1;
		this.op2 = op2;
		this.op3 = op3;
	}

	public Block() {
		opcode = null;
		op1 = null;
		op2 = null;
		op3 = null;
	}

	public String headerString() {
		return "Opcode\tSR\tVR\tPR\tNU\tSR\tVR\tPR\tNU\tSR\tVR\tPR\tNU";
	}

	public String toStringNorm() {
		switch (opcode.getOpType()) {
		case ARITHOP:
			return opcode + "\tr" + op1.sr + "\t,\tr" + op2.sr + "\t=>\tr" + op3.sr;
		case LOADI:
			return opcode + "\t" + op1.pr + "\t\t\t=>\tr" + op3.sr;
		case MEMOP:
			if (opcode == OPCODE.LOAD) {
				return opcode + "\tr" + op1.sr + "\t\t\t=>\tr" + op3.sr;
			} else if (opcode == OPCODE.STORE) {
				return opcode + "\tr" + op1.sr + "\t=>\tr" + op2.sr;
			}
			break;
		case NOP:
			break;
		case OUTPUT:
			return opcode + "\t" + op1.pr;
		default:
			break;
		}
		return "";
	}

	public String toString() {
		return opcode + "\t" + op1.toString() + "   |   " + op2.toString() + "   |   " + op3.toString();
	}

	public String toPRString() {
		String x = op1.pr == -1 ? "" : (opcode == OPCODE.LOADI || opcode == OPCODE.OUTPUT ? "" : "r") + op1.pr;
		String y = op2.pr == -1 ? "" : "r" + op2.pr;
		String z = op3.pr == -1 ? "" : "r" + op3.pr;

		String commaCheck = "";
		if (opcode.getOpType() == OPTYPE.ARITHOP) {
			commaCheck = ",";
		}
		String comma = String.format("%-5s", commaCheck);

		String rocketCheck = "";
		if (opcode.getOpType() != OPTYPE.OUTPUT) {
			rocketCheck = "=>";
		}
		String rocket = String.format("%-5s", rocketCheck);

		String code = String.format("%-10s", opcode);
		String first = String.format("%-10s", x);
		String second = String.format("%-10s", y);
		String third = String.format("%-10s", z);

		if (opcode == OPCODE.STORE) {
			return code + first + comma + third + rocket + second;
		}
		return code + first + comma + second + rocket + third;

	}
}
