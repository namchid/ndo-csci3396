# README for parser #

### Make commands ###

```
#!bash

make
make clean
make run
```

### Running Parser ###

Order of flags and filename does not matter:
```
#!bash

./llgen <flags*> <filename> <flags*>
```
where valid flags are:
```
#!bash

-h  print help
-t  print table
-s  print output in YAML format
```

### Redirecting outputs to file ###

```
#!bash
./pipeOutputsToFile
```

### Running good tests ###

```
#!bash
./goodTests
```

### Full sequence for testing, because I will forget later ###

```
#!bash
make clean
make
./pipeOutputsToFile
./goodTests
./badTests
```